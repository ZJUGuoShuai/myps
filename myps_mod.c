/**
 * myps_mod - A kernel module used to display the statistics of
 *     all processes in your host.
 * 
 * Author: Guo Shuai
 * Date:   2018/10/12
 */

#include <linux/kernel.h>       // KERN_INFO
#include <linux/module.h>       // 编写 Kernel module 所需
#include <linux/sched.h>        // task_struct 结构体
#include <linux/sched/signal.h> // for_each_process 宏

/* 打印所有进程的信息 */
void procs_info_print(void) {
    struct task_struct* task_list; // 系统进程列表

    // 各种状态进程的计数器
    size_t process_counter = 0;
    size_t running_counter = 0;
    size_t inter_counter = 0;
    size_t uninter_counter = 0;
    size_t zombie_counter = 0;
    size_t stopped_counter = 0;
    size_t dead_counter = 0;
    size_t idle_counter = 0;

    // 打印表头
    pr_info("%s %5s  %8s %16s -> %16s\n", " ", "PID", "State", "Process name", "Parent name");

    // 通过 for_each_process() 宏来遍历系统进程
    for_each_process(task_list) {
        char state[8];   // 表示进程状态的字符串
        char symbol[16]; // 通过 ANSI 控制字符显示不同颜色的状态符号

        // 通过 task_list->state 判断此进程状态，以此设定状态字符串、状态符号和计数器
        switch (task_list->state) {
            case TASK_RUNNING:
                strcpy(state, "RUNNING");
                strcpy(symbol, "\033[32m●\033[0m");
                running_counter++;
                break;
            case TASK_STOPPED:
                strcpy(state, "STOPPED");
                strcpy(symbol, "\033[31m●\033[0m");
                stopped_counter++;
                break;
            case TASK_INTERRUPTIBLE:
                strcpy(state, "INTER");
                strcpy(symbol, "\033[34m●\033[0m");
                inter_counter++;
                break;
            case TASK_UNINTERRUPTIBLE:
                strcpy(state, "UNINTER");
                strcpy(symbol, "\033[35m●\033[0m");
                uninter_counter++;
                break;
            case TASK_DEAD:
                strcpy(state, "DEAD");
                strcpy(symbol, "\033[38m●\033[0m");
                dead_counter++;
                break;
            case TASK_IDLE:
                strcpy(state, "IDLE");
                strcpy(symbol, "\033[37m●\033[0m");
                idle_counter++;
                break;
            case EXIT_ZOMBIE:
                strcpy(state, "ZOMBIE");
                strcpy(symbol, "\033[35;1m●\033[0m");
                zombie_counter++;
                break;
            
            // 其他未知状态
            default:
                strcpy(state, "?");
        }

        // 打印这一进程的信息
        printk(KERN_INFO "%s %5d  %8s %16s -> %16s\n", symbol, task_list->pid, state, task_list->comm, task_list->real_parent->comm);

        // 总进程数+1
        ++process_counter;
    }

    // 打印统计信息
    printk(KERN_INFO "==== MyPS Statistics ====\n");
    printk(KERN_INFO "\033[32m%4ld\033[0m Running processes\n", running_counter);
    printk(KERN_INFO "\033[31m%4ld\033[0m Stopped processes\n", stopped_counter);
    printk(KERN_INFO "\033[37m%4ld\033[0m Idle processes\n", idle_counter);
    printk(KERN_INFO "\033[34m%4ld\033[0m Interruptible processes\n", inter_counter);
    printk(KERN_INFO "\033[35m%4ld\033[0m Uninterruptible processes\n", uninter_counter);
    printk(KERN_INFO "\033[38m%4ld\033[0m Dead processes\n", dead_counter);
    printk(KERN_INFO "\033[31;1m%4ld\033[0m Zombie processes\n", zombie_counter);
    printk(KERN_INFO "-------------------------\n");
    printk(KERN_INFO "%4ld Processes\n", process_counter);

    // 打印结束语句
    printk(KERN_INFO "MyPS ended.\n");
}

/* Kernel module 基本单元 1: init_module() */
int init_module(void) {
    printk(KERN_INFO "MyPS loaded.\n");

    procs_info_print();

    return 0;
}

/* Kernel module 基本单元 2: cleanup_module() */
void cleanup_module(void) {
    printk(KERN_INFO "MyPS unloaded.\n");
}

/* 其他模块元信息 */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Guo Shuai");
MODULE_DESCRIPTION("A Process Statistic Module");