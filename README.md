# MyPS

理论上支持所有使用 syslog/syslog-ng 的系统。

经过测试的发行版：
- Manjaro 18.0.0-rc (syslog-ng)
- Ubuntu 18.04 (syslog)

## Kernel module

### 编译

在此目录下运行：
```
$ make
```

### 安装模块

在此目录下运行：
```
$ sudo insmod myps_mod.ko
```

### 卸载模块

```
$ sudo rmmod myps_mod
```

### 清理编译结果

```
$ make clean
```

## 客户端（User-space program）

在`client`目录。
```
$ make      # 编译
$ sudo ./myps # 运行
```