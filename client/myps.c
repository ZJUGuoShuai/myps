/**
 * MyPS Client - A user-space program used to display the output of myps_mod.
 * 
 * Author: Guo Shuai
 * Date:   2018/10/12
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int main()
{
    FILE *fp; // 全局文件指针

    // 1. 尝试打开系统日志文件，若打不开则报错并退出
    if ((fp = fopen("/var/log/kernel.log", "r")) == NULL &&
        (fp = fopen("/var/log/kern.log", "r")) == NULL)
    {
        perror("Cannot open kernel log.\n"
               "Check if you have /var/log/kern.log (from syslog)\n"
               "or /var/log/kernel.log (from syslog-ng)\n");
        exit(-1);
    }

    // 2. 倒着读文件，找到最后一次MyPS输出的地方

    char line[256];         // 用于存储文件中每一行
    char ch;                // 当前文件指针指向的字符
    int offset;             // 日志文件中每行日志的前缀长度
    fseek(fp, 0, SEEK_END); // 将文件定位到末尾
    while (ch = getc(fp))
    {
        // 读取文件的每一行
        if ((ch == '\n' || ftell(fp) == 1) && fgets(line, sizeof(line), fp))
        {
            bool isHead = false; // 是否是MyPS的开头
            for (offset = 0; offset < strlen(line); offset++)
            {
                if (strcmp(line + offset, "MyPS loaded.\n") == 0)
                {
                    isHead = true;
                    
                    // 如果是第一行，则offset会少1，所以要加上
                    if (ftell(fp) == 1)
                        offset++;
                    
                    break;
                }
            }

            // 若 line 是 MyPS 输出的开头，即含有 "MyPS loaded.\n"，则说明找到了，
            // 并跳出循环
            if (isHead)
                break;

            // 这一行不是开头
            // 若已经读到了文件开头，说明现在系统日志中还没有过MyPS的输出，则退出程序
            if (ftell(fp) == 1)
            {
                printf("No MyPS log the in kernel log.\n");
                return 0;
            }

            // 将文件指针归位
            fseek(fp, -(strlen(line) + 1), SEEK_CUR);
        }

        // 将文件指针移到前一个没有读过的字符上
        fseek(fp, -2, SEEK_CUR);
    }

    // 3. 将 MyPS 的内容输出
    while (fgets(line, sizeof(line), fp))
    {
        if (strcmp(line + offset, "MyPS ended.\n") == 0)
            break;

        // 打印 MyPS 的一行输出
        printf("%s", line + offset);
    }

    // 关闭文件
    fclose(fp);

    return 0;
}
